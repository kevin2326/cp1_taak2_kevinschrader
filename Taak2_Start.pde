//Kevin Schrader
//kevin.schrader@student.ehb.be
//Multec 
//Academiejaar: 2017-2018


//Coördinates of cube
int middleX, middleY, top, bottom, left, right, topCorner, bottomCorner;

//Variables for myAnimation
int size = 10;
boolean playMyAnimation = false;
boolean playMyAnimation2 = false;
//de animatie 3 word altijd getoond
boolean playMyAnimation3 = true;
boolean playMyAnimation4 = false;
boolean playMyAnimation5 = false;

void setup() {
  size(1280, 720); 
  colorMode(HSB, 322, 322, 100, 86);
  setGlobalVariables();
  initializeMinim();
}

void draw() {
  //White Cube planes
  background(0, 0, 100);
  //checks each frame if there is a beat.
  beat.detect(player.mix);

  drawAnimations();
  drawCubeMask();
}

void keyReleased() {
  //toggle Animations ON or OFF
  //animatie 3 wordt uitgezet wanneer er een andere animatie wordt getoond
  if (key == 'u') {
    playMyAnimation3 = !playMyAnimation3;
    playMyAnimation = !playMyAnimation;
  }
  if (key == 'i') {
    playMyAnimation3 = !playMyAnimation3;
    playMyAnimation2 = !playMyAnimation2;
  }
  if (key == 'o') {
    playMyAnimation3 = !playMyAnimation3;
    playMyAnimation4 = !playMyAnimation4;
  }
  if (key == 'j') {
    playMyAnimation3 = !playMyAnimation3;
    playMyAnimation5 = !playMyAnimation5;
  }
}

void drawAnimations()
{  
  //play animation if toggled ON
  if (playMyAnimation == true) {
    myAnimation();
  }
  if (playMyAnimation2 == true) {
    myAnimation2();
  }
  if (playMyAnimation3 == true) {
    myAnimation3();
  }
  if (playMyAnimation4 == true) {
    myAnimation4();
  }
  if (playMyAnimation5 == true) {
    myAnimation5();
  }
}
//hier wordt er een cirkel getoond
void myAnimation() {
  if (beat.isOnset()) {
    size = 10;
  }

  noStroke();
  fill(frameCount % 360, 100, 50);
  ellipse(middleX, middleY, size, size);

  size += 20;
}
//hier worden diagonale cirkels getoond links vanonder
void myAnimation2() {
  if (beat.isOnset()) {
    size = 10;
  }
  noStroke();
  fill(frameCount % 170, 100, 100);
  for (int i = 0; i < 30; i+=2) {
    ellipse(topCorner + (i *20), topCorner + (i*20), size/8, size/8);
  }
  size+= 10;
}
//hier wordt de cirkel in het midden een vierkant
void myAnimation3() {
  if (beat.isOnset()) {
    size = 10;
  }
  noStroke();
  fill(frameCount % 170, 100, 86);
  rectMode(CENTER);
  rect(middleX, middleY, size, size);
  size+= 20;
}
//hier worden er ook links vanonder diagonale cirkels getoond
//maar enkel met een uitlijn en geen opvulkleur
void myAnimation4() {
  if (beat.isOnset()) {
    size = 10;
  }
  noFill();
  stroke(1);
  for (int i = 0; i < 30; i+=2) {
    ellipse(bottom - (i * 10), bottom - (i * 10), size/10, size/10);
  }

  size+= 5;
}
//hierbij worden er bovenaan 200 cirkels getoond 
//ik heb mij hier gebaseerd op oefening 9.4 van les 9
void myAnimation5() {
  if (beat.isOnset()) {
    size = 1;
  }
  int[] getallen = new int[200];
  for (int i = 0; i <200; i++) {
    fill(frameCount % 170, 100, 80);
    getallen[i] = round(random(0, 200));
    ellipse(topCorner + i*20, getallen[i] + 10, size, size);
    
    noStroke();
  }
  size += 1;
}
//wanneer er met de muis gesleept wordt word de grootte 100
void mouseDragged() 
{
  size = 100;
}